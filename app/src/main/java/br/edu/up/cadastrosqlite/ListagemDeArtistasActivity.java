package br.edu.up.cadastrosqlite;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import br.edu.up.cadastrosqlite.dominio.Endereco;
import br.edu.up.cadastrosqlite.dominio.Pessoa;

public class ListagemDeArtistasActivity extends AppCompatActivity {

    private SQLiteDatabase db;
    private ArrayList<String> nomes;
    private ArrayAdapter<String> adapter;
    private ArrayList<Pessoa> listaDePessoas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = openOrCreateDatabase("banco.db", MODE_PRIVATE, null);

        db.execSQL("CREATE TABLE IF NOT EXISTS pessoas (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, nome TEXT)");
        db.execSQL("CREATE TABLE IF NOT EXISTS enderecos (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "id_pessoa INTEGER, rua TEXT,  numero TEXT, bairro TEXT)");

        listaDePessoas = new ArrayList<>();
        Cursor resultado = db.rawQuery("select * from pessoas",null);
        nomes = new ArrayList<>();
        while(resultado.moveToNext()){
            int id = resultado.getInt(resultado.getColumnIndex("id"));
            String nome = resultado.getString(resultado.getColumnIndex("nome"));
            Pessoa pessoa = new Pessoa(id, nome);
            listaDePessoas.add(pessoa);
            nomes.add(nome);
        }

        adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_list_item_1, nomes);
        ListView listView = (ListView) findViewById(R.id.listViewPessoas);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Pessoa pessoa = listaDePessoas.get(position);
                int idPessoa = pessoa.getId();
                String[] params = new String[]{String.valueOf(idPessoa)};

                Cursor resultado = db.rawQuery("SELECT * FROM enderecos WHERE id_pessoa = ?", params);

                ArrayList<Endereco> listaDeEnderecos = new ArrayList<Endereco>();
                while (resultado.moveToNext()) {
                    int idEndereco = resultado.getInt(resultado.getColumnIndex("id"));
                    String rua = resultado.getString(resultado.getColumnIndex("rua"));
                    String nro = resultado.getString(resultado.getColumnIndex("numero"));
                    String bairro = resultado.getString(resultado.getColumnIndex("bairro"));

                    Endereco endereco = new Endereco(idEndereco, idPessoa, rua, nro, bairro);
                    listaDeEnderecos.add(endereco);
                }

                if (listaDeEnderecos.size() > 0) {
                    Intent in = new Intent(ListagemDeArtistasActivity.this, ListagemDeEnderecosActivity.class);
                    in.putExtra("pessoa", pessoa);
                    in.putExtra("enderecos", listaDeEnderecos);
                    startActivity(in);
                } else {
                    Intent in = new Intent(ListagemDeArtistasActivity.this, CadastroDeEnderecosActivity.class);
                    in.putExtra("pessoa", pessoa);
                    in.putExtra("endereco", new Endereco());
                    startActivity(in);
                }
            }
        });


        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                final Pessoa pessoa = listaDePessoas.get(position);
                final String pessoaAdapter = nomes.get(position);

                String[] acoes = {"Alterar", "Excluir", "Cancelar"};
                AlertDialog.Builder builder = new AlertDialog.Builder(ListagemDeArtistasActivity.this);
                builder.setTitle("O que você deseja fazer?")
                        .setItems(acoes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case 0:
                                        alterar(pessoa);
                                        break;
                                    case 1:
                                        excluir(pessoa, pessoaAdapter);
                                        break;
                                }
                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
                return false;
            }
        });
    }

    private void alterar(Pessoa pessoa){
        Intent in = new Intent(this, CadastroDePessoaActivity.class);
        in.putExtra("pessoa", pessoa);
        startActivity(in);
    }

    private void excluir(final Pessoa pessoa, final String pessoaAdapter){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Tem certeza que deseja excluir?");
        builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Cursor resultado = db.rawQuery("SELECT id FROM enderecos WHERE id_pessoa = ?", new String[]{String.valueOf(pessoa.getId())});
                if (resultado.getCount() > 0) {
                    Toast.makeText(ListagemDeArtistasActivity.this, "Remova os endereços primeiro!", Toast.LENGTH_LONG).show();
                } else {
                    db.execSQL("DELETE FROM pessoas WHERE id = ?",
                            new String[]{String.valueOf(pessoa.getId())}
                    );
                    adapter.remove(pessoaAdapter);
                    adapter.notifyDataSetChanged();
                }
            }
        });
        builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // Não faz nada.
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.inicio) {
            Intent in = new Intent(this, ListagemDeArtistasActivity.class);
            startActivity(in);
            return true;
        }

        if (id == R.id.incluir) {
            Intent in = new Intent(this, CadastroDePessoaActivity.class);
            in.putExtra("pessoa", new Pessoa());
            startActivity(in);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
