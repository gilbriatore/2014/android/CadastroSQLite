package br.edu.up.cadastrosqlite;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import br.edu.up.cadastrosqlite.dominio.Endereco;
import br.edu.up.cadastrosqlite.dominio.Pessoa;

public class CadastroDeEnderecosActivity extends AppCompatActivity {

    private Pessoa pessoa;
    private Endereco endereco;
    private SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_de_enderecos);

        db = openOrCreateDatabase("banco.db", MODE_PRIVATE, null);

        Intent in = getIntent();
        pessoa = (Pessoa) in.getSerializableExtra("pessoa");
        endereco = (Endereco) in.getSerializableExtra("endereco");

        TextView textView = (TextView) findViewById(R.id.txtPessoa);
        textView.setText(pessoa.getNome());

        TextView txtRua = (TextView) findViewById(R.id.txtRua);
        TextView txtNumero = (TextView) findViewById(R.id.txtNumero);
        TextView txtBairro = (TextView) findViewById(R.id.txtBairro);
        txtRua.setText(endereco.getRua());
        txtNumero.setText(endereco.getNumero());
        txtBairro.setText(endereco.getBairro());
    }

    public void gravar(View view) {

        TextView txtRua = (TextView) findViewById(R.id.txtRua);
        TextView txtNumero = (TextView) findViewById(R.id.txtNumero);
        TextView txtBairro = (TextView) findViewById(R.id.txtBairro);
        String rua = txtRua.getText().toString();
        String numero = txtNumero.getText().toString();
        String bairro = txtBairro.getText().toString();

        if (endereco.getId() > 0){
            String[] valores = {rua, numero, bairro, String.valueOf(endereco.getId())};
            db.execSQL("UPDATE enderecos SET rua = ?, numero = ?, bairro = ? WHERE id = ?", valores);
        } else {
            String[] valores = {String.valueOf(pessoa.getId()), rua, numero, bairro};
            db.execSQL("INSERT INTO enderecos (id_pessoa, rua, numero, bairro) values (?,?,?,?)", valores);
        }

        String[] params =  new String[]{String.valueOf(pessoa.getId())};
        Cursor resultado = db.rawQuery("SELECT * FROM enderecos WHERE id_pessoa = ?", params);

        ArrayList<Endereco> listaDeEnderecos = new ArrayList<>();
        while(resultado.moveToNext()){
            int idEndereco = resultado.getInt(resultado.getColumnIndex("id"));
            String ruadb = resultado.getString(resultado.getColumnIndex("rua"));
            String nro = resultado.getString(resultado.getColumnIndex("numero"));
            String bairrodb = resultado.getString(resultado.getColumnIndex("bairro"));

            Endereco endereco = new Endereco(idEndereco, pessoa.getId(), ruadb, nro, bairrodb);
            listaDeEnderecos.add(endereco);
        }

        Intent in = new Intent(this, ListagemDeEnderecosActivity.class);
        in.putExtra("pessoa", pessoa);
        in.putExtra("enderecos", listaDeEnderecos);
        startActivity(in);
    }
}
