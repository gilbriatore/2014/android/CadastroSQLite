package br.edu.up.cadastrosqlite.dominio;

import java.io.Serializable;

/**
 * Universidade Positivo
 * Bacharelado em Sistemas de Informação
 * Criado por Geucimar Brilhador
 * 15/10/2015
 */
public class Endereco implements Serializable {

    private int id;
    private int idPessoa;
    private String rua;
    private String numero;
    private String bairro;

    public Endereco(){
    }

    public Endereco(int id, int idPessoa, String rua, String numero, String bairro) {
        this.id = id;
        this.idPessoa = idPessoa;
        this.rua = rua;
        this.numero = numero;
        this.bairro = bairro;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }
}
