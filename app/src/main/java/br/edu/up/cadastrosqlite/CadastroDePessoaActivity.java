package br.edu.up.cadastrosqlite;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import br.edu.up.cadastrosqlite.dominio.Pessoa;

public class CadastroDePessoaActivity extends AppCompatActivity {

    private Pessoa pessoa;
    private SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_de_pessoa);
        db = openOrCreateDatabase("banco.db", MODE_PRIVATE, null);

        Intent in = getIntent();
        pessoa = (Pessoa) in.getSerializableExtra("pessoa");

        TextView textView = (TextView) findViewById(R.id.txtNome);
        textView.setText(pessoa.getNome());
    }

    public void gravar(View view) {

        TextView textView = (TextView) findViewById(R.id.txtNome);
        String nome = textView.getText().toString();

        if (pessoa.getId() > 0){
            String[] valores = {nome, String.valueOf(pessoa.getId())};
            db.execSQL("UPDATE pessoas SET nome = ? WHERE id = ?", valores);
        } else {
            db.execSQL("INSERT INTO pessoas (nome) VALUES (?)", new String[]{nome});
        }

        Intent in = new Intent(this, ListagemDeArtistasActivity.class);
        startActivity(in);
    }
}
