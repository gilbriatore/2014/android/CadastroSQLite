package br.edu.up.cadastrosqlite;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import br.edu.up.cadastrosqlite.dominio.Endereco;
import br.edu.up.cadastrosqlite.dominio.Pessoa;

public class ListagemDeEnderecosActivity extends AppCompatActivity {

    private Pessoa pessoa;
    private SQLiteDatabase db;
    private ArrayList<String> enderecos;
    private ArrayList<Endereco> listaDeEnderecos;
    private ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listagem_de_enderecos);

        db = openOrCreateDatabase("banco.db", MODE_PRIVATE, null);

        Intent in = getIntent();
        pessoa = (Pessoa) in.getSerializableExtra("pessoa");
        listaDeEnderecos = (ArrayList<Endereco>) in.getSerializableExtra("enderecos");

        enderecos = new ArrayList<>();
        for(Endereco endereco : listaDeEnderecos){
            String str = endereco.getRua() + ", " + endereco.getNumero() + " - " + endereco.getBairro();
            enderecos.add(str);
        }

        TextView txtPessoaEndereco = (TextView) findViewById(R.id.txtEnderecos);
        txtPessoaEndereco.setText("Endereços do(a) " + pessoa.getNome());

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, enderecos);
        ListView listView = (ListView) findViewById(R.id.listViewEnderecos);
        listView.setAdapter(adapter);

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                final Endereco endereco = listaDeEnderecos.get(position);
                final String enderecoAdapter =  enderecos.get(position);
                String[] acoes = {"Alterar", "Excluir", "Cancelar"};
                AlertDialog.Builder builder = new AlertDialog.Builder(ListagemDeEnderecosActivity.this);
                builder.setTitle("O que você deseja fazer?")
                        .setItems(acoes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case 0:
                                        alterar(endereco);
                                        break;
                                    case 1:
                                        excluir(endereco, enderecoAdapter);
                                        break;
                                }
                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
                return false;
            }
        });


    }

    private void alterar(Endereco endereco){
        Intent in = new Intent(this, CadastroDeEnderecosActivity.class);
        in.putExtra("pessoa", pessoa);
        in.putExtra("endereco", endereco);
        startActivity(in);
    }

    private void excluir(final Endereco endereco, final String enderecoAdapter){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Tem certeza que deseja excluir?");
        builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                db.execSQL("DELETE FROM enderecos WHERE id = ?",
                        new String[]{String.valueOf(endereco.getId())}
                );
                adapter.remove(enderecoAdapter);
                adapter.notifyDataSetChanged();
            }
        });
        builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // Não faz nada.
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_listagem_de_enderecos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.inicio) {
            Intent in = new Intent(this, ListagemDeArtistasActivity.class);
            startActivity(in);
            return true;
        }

        if (id == R.id.incluir) {
            Intent in = new Intent(this, CadastroDeEnderecosActivity.class);
            in.putExtra("pessoa", pessoa);
            in.putExtra("endereco", new Endereco());
            startActivity(in);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
