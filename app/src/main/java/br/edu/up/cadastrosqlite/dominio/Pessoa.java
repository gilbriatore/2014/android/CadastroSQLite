package br.edu.up.cadastrosqlite.dominio;

import java.io.Serializable;

/**
 * Universidade Positivo
 * Bacharelado em Sistemas de Informação
 * Criado por Geucimar Brilhador
 * 15/10/2015
 */
public class Pessoa implements Serializable {

    private int id;
    private String nome;

    public Pessoa(){
    }

    public Pessoa(int id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
